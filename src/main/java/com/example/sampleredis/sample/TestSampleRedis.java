package com.example.sampleredis.sample;

import com.example.sampleredis.service.redis.RedisService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
@Log4j2
public class TestSampleRedis implements CommandLineRunner {
    @Autowired
    private RedisService redisService;

    private static final String key1 = "keyone";
    private static final String value1 = "123456";
    private static final String key2 = "keytwo";
    private static final String value2 = "value two";

    @Override
    public void run(String... args) throws Exception {
        testRedis();
    }

    public void testRedis() {
        log.info("------ Test Redis ------");
        redisService.set(key1, value1);
        String result1 =  redisService.get(key1);
        log.info("Value of key 1 : {}", result1);
        redisService.set(key2, value2);
        String result2 = (String) redisService.get(key2);
        log.info("Value of key 2 : {}", result2);
    }
}
