package com.example.sampleredis.service.redis;

import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisHash;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

@Service
@RedisHash(value = "RedisService", timeToLive = 36_000_000)
@Log4j2
public class RedisService {
    @Autowired
    private RedisTemplate redisTemplate;

    public void set(String key, String value){
        try {
            redisTemplate.opsForValue().set((String)key,(String)value);
            log.debug("redis save|| Key: {} -- Value: {}",key,value);
        } catch (Exception e){
            log.error("redis error: {}", e.getMessage());
            e.printStackTrace();
        }
    }

    public String get(String key){
        try {
            log.debug("Get Value From Key: {} -- Value {}",key, redisTemplate.opsForValue().get(key));
            return (String) redisTemplate.opsForValue().get(key);
        } catch (Exception e){
            log.error("redis error: {}", e.getMessage());
            e.printStackTrace();
            return null;
        }
    }
}
